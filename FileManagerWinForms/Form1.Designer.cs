﻿namespace FileManagerWinForms
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.directoryTree = new System.Windows.Forms.TreeView();
            this.fileContentNameBox = new System.Windows.Forms.TextBox();
            this.contentBox = new System.Windows.Forms.TextBox();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.countFilesStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.fileNameStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.timeStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer = new System.Windows.Forms.Timer(this.components);
            this.buttonStop = new System.Windows.Forms.Button();
            this.buttonSearch = new System.Windows.Forms.Button();
            this.buttonPauseResume = new System.Windows.Forms.Button();
            this.fileNameBox = new System.Windows.Forms.TextBox();
            this.drectoryNameBox = new System.Windows.Forms.TextBox();
            this.buttonContentSearch = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.fileNameLabel = new System.Windows.Forms.Label();
            this.statusStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // directoryTree
            // 
            this.directoryTree.Location = new System.Drawing.Point(12, 223);
            this.directoryTree.Name = "directoryTree";
            this.directoryTree.Size = new System.Drawing.Size(262, 314);
            this.directoryTree.TabIndex = 0;
            this.directoryTree.NodeMouseClick += new System.Windows.Forms.TreeNodeMouseClickEventHandler(this.ClickOnNode);
            // 
            // fileContentNameBox
            // 
            this.fileContentNameBox.Location = new System.Drawing.Point(12, 103);
            this.fileContentNameBox.Multiline = true;
            this.fileContentNameBox.Name = "fileContentNameBox";
            this.fileContentNameBox.Size = new System.Drawing.Size(262, 50);
            this.fileContentNameBox.TabIndex = 1;
            // 
            // contentBox
            // 
            this.contentBox.Location = new System.Drawing.Point(280, 64);
            this.contentBox.Multiline = true;
            this.contentBox.Name = "contentBox";
            this.contentBox.ReadOnly = true;
            this.contentBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.contentBox.Size = new System.Drawing.Size(508, 473);
            this.contentBox.TabIndex = 2;
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.countFilesStatusLabel,
            this.fileNameStatusLabel,
            this.timeStatusLabel});
            this.statusStrip.Location = new System.Drawing.Point(0, 540);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(800, 22);
            this.statusStrip.TabIndex = 5;
            this.statusStrip.Text = "statusStrip1";
            // 
            // countFilesStatusLabel
            // 
            this.countFilesStatusLabel.Name = "countFilesStatusLabel";
            this.countFilesStatusLabel.Size = new System.Drawing.Size(13, 17);
            this.countFilesStatusLabel.Text = "0";
            // 
            // fileNameStatusLabel
            // 
            this.fileNameStatusLabel.Name = "fileNameStatusLabel";
            this.fileNameStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // timeStatusLabel
            // 
            this.timeStatusLabel.Name = "timeStatusLabel";
            this.timeStatusLabel.Size = new System.Drawing.Size(772, 17);
            this.timeStatusLabel.Spring = true;
            this.timeStatusLabel.Text = "00:00:00";
            this.timeStatusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonStop
            // 
            this.buttonStop.Location = new System.Drawing.Point(713, 12);
            this.buttonStop.Name = "buttonStop";
            this.buttonStop.Size = new System.Drawing.Size(75, 23);
            this.buttonStop.TabIndex = 6;
            this.buttonStop.Text = "Остановить";
            this.buttonStop.UseVisualStyleBackColor = true;
            this.buttonStop.Click += new System.EventHandler(this.ClickOnButtonStop);
            // 
            // buttonSearch
            // 
            this.buttonSearch.Location = new System.Drawing.Point(12, 159);
            this.buttonSearch.Name = "buttonSearch";
            this.buttonSearch.Size = new System.Drawing.Size(107, 23);
            this.buttonSearch.TabIndex = 7;
            this.buttonSearch.Text = "Поиск";
            this.buttonSearch.UseVisualStyleBackColor = true;
            this.buttonSearch.Click += new System.EventHandler(this.ClickOnButtonSearch);
            // 
            // buttonPauseResume
            // 
            this.buttonPauseResume.Location = new System.Drawing.Point(657, 12);
            this.buttonPauseResume.Name = "buttonPauseResume";
            this.buttonPauseResume.Size = new System.Drawing.Size(50, 23);
            this.buttonPauseResume.TabIndex = 8;
            this.buttonPauseResume.Text = "▌ ▌";
            this.buttonPauseResume.UseVisualStyleBackColor = true;
            this.buttonPauseResume.Click += new System.EventHandler(this.ClickOnButtonPauseResume);
            // 
            // fileNameBox
            // 
            this.fileNameBox.Location = new System.Drawing.Point(12, 64);
            this.fileNameBox.Name = "fileNameBox";
            this.fileNameBox.Size = new System.Drawing.Size(262, 20);
            this.fileNameBox.TabIndex = 9;
            // 
            // drectoryNameBox
            // 
            this.drectoryNameBox.Location = new System.Drawing.Point(12, 25);
            this.drectoryNameBox.Name = "drectoryNameBox";
            this.drectoryNameBox.Size = new System.Drawing.Size(262, 20);
            this.drectoryNameBox.TabIndex = 10;
            // 
            // buttonContentSearch
            // 
            this.buttonContentSearch.Location = new System.Drawing.Point(125, 159);
            this.buttonContentSearch.Name = "buttonContentSearch";
            this.buttonContentSearch.Size = new System.Drawing.Size(149, 23);
            this.buttonContentSearch.TabIndex = 11;
            this.buttonContentSearch.Text = "Поиск по содержимому";
            this.buttonContentSearch.UseVisualStyleBackColor = true;
            this.buttonContentSearch.Click += new System.EventHandler(this.ClickOnButtonContentSearch);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 87);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Содержимое файла:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 13);
            this.label2.TabIndex = 13;
            this.label2.Text = "Директория поиска:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 48);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(95, 13);
            this.label3.TabIndex = 14;
            this.label3.Text = "Название файла:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(9, 207);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Дерево каталогов:";
            // 
            // fileNameLabel
            // 
            this.fileNameLabel.AutoSize = true;
            this.fileNameLabel.Location = new System.Drawing.Point(280, 48);
            this.fileNameLabel.Name = "fileNameLabel";
            this.fileNameLabel.Size = new System.Drawing.Size(0, 13);
            this.fileNameLabel.TabIndex = 16;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 562);
            this.Controls.Add(this.fileNameLabel);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.buttonContentSearch);
            this.Controls.Add(this.drectoryNameBox);
            this.Controls.Add(this.fileNameBox);
            this.Controls.Add(this.buttonPauseResume);
            this.Controls.Add(this.buttonSearch);
            this.Controls.Add(this.buttonStop);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.contentBox);
            this.Controls.Add(this.fileContentNameBox);
            this.Controls.Add(this.directoryTree);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FileManager";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.CloseForm);
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TreeView directoryTree;
        private System.Windows.Forms.TextBox fileContentNameBox;
        private System.Windows.Forms.TextBox contentBox;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.ToolStripStatusLabel countFilesStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel fileNameStatusLabel;
        private System.Windows.Forms.ToolStripStatusLabel timeStatusLabel;
        private System.Windows.Forms.Timer timer;
        private System.Windows.Forms.Button buttonStop;
        private System.Windows.Forms.Button buttonSearch;
        private System.Windows.Forms.Button buttonPauseResume;
        private System.Windows.Forms.TextBox fileNameBox;
        private System.Windows.Forms.TextBox drectoryNameBox;
        private System.Windows.Forms.Button buttonContentSearch;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label fileNameLabel;
    }
}

