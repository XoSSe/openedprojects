﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FileManagerWinForms
{
    public partial class Form1 : Form
    {
        int countFiles;     // Счётчик отсканированных файлов

        bool pause = false;         // Состояние кнопки паузы
        bool statusSearch = false;  // Результат поиска файла в текущей ветке дерева

        string path;    // Директория поиска

        DateTime date;          // Секундомер
        Thread searchThread;    // Поток поиска

        ManualResetEvent mrse;  // Событие синхронизации потоков

        /// <summary>
        /// Конструктор формы Form1
        /// </summary>
        public Form1()
        {
            InitializeComponent();

            mrse = new ManualResetEvent(true);

            path = Properties.Settings.Default.defaultPath;
            fileNameBox.Text = Properties.Settings.Default.defaultFileName;
            fileContentNameBox.Text = Properties.Settings.Default.defaultText;

            drectoryNameBox.Text = path;

            StartSearch();
        }

        /// <summary>
        /// Метод выполнения одного такта таймера
        /// </summary>
        /// <param name="sender">Объект таймера</param>
        /// <param name="e">Событие таймера</param>
        private void TickTimer(object sender, EventArgs e)
        {
            date = date.AddSeconds(1);
            timeStatusLabel.Text = date.ToString("HH:mm:ss");
        }

        /// <summary>
        /// Метод запуска потока сканирования директории
        /// </summary>
        private void StartSearch()
        {
            date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            timeStatusLabel.Text = date.ToString("HH:mm:ss");

            countFiles = 0;

            if (drectoryNameBox.Text != "")
                path = drectoryNameBox.Text;

            buttonStop.Enabled = true;
            buttonSearch.Enabled = false;
            buttonPauseResume.Enabled = true;
            buttonContentSearch.Enabled = false;

            SetDefaultPause();

            timer = new System.Windows.Forms.Timer()
            {
                Interval = 1000
            };
            timer.Tick += TickTimer;
            timer.Start();

            searchThread = new Thread(new ThreadStart(DirectorySearch));
            searchThread.Start();
        }

        /// <summary>
        /// Метод запуска сканирования директории в отдельном потоке
        /// </summary>
        public void DirectorySearch()
        {
            mrse.WaitOne();

            TreeNode directory = new TreeNode(path);

            if (directoryTree.InvokeRequired)
            {
                directoryTree.Invoke(new Action<TreeNode>((s) => directoryTree.Nodes.Add(s)), directory);
                Console.WriteLine(path);

                DirectorySearch(directory, path);

                fileNameStatusLabel.Text = "Поиск завершён";

                timer.Stop();

                buttonStop.BeginInvoke(new Action<bool>((s) => buttonStop.Enabled = s), false);
                buttonSearch.BeginInvoke(new Action<bool>((s) => buttonSearch.Enabled = s), true);
                buttonPauseResume.BeginInvoke(new Action<bool>((s) => buttonPauseResume.Enabled = s), false);
                buttonContentSearch.BeginInvoke(new Action<bool>((s) => buttonContentSearch.Enabled = s), true);
            }
        }

        /// <summary>
        ///  Метод сканирования директории
        /// </summary>
        /// <param name="node">Текущий узел дерева директорий</param>
        /// <param name="directory">Текущая директория сканирования</param>
        public void DirectorySearch(TreeNode node, string directory)
        {
            mrse.WaitOne();

            try
            {
                if (directoryTree.InvokeRequired)
                {
                    foreach (string d in Directory.GetDirectories(directory))
                    {
                        mrse.WaitOne();

                        statusSearch = false;

                        TreeNode dir = new TreeNode();
                        dir.Text = Path.GetFileName(d);

                        DirectorySearch(dir, d);

                        if (fileNameBox.Text != "")
                        {
                            if (statusSearch)
                                directoryTree.Invoke(new Action<TreeNode>((s) => node.Nodes.Add(s)), dir);
                        }
                        else
                        {
                            directoryTree.Invoke(new Action<TreeNode>((s) => node.Nodes.Add(s)), dir);
                        }
                    }

                    foreach (string f in Directory.GetFiles(directory))
                    {
                        mrse.WaitOne();

                        if (fileNameBox.Text != "")
                        {
                            if (Path.GetFileName(f) == fileNameBox.Text)
                            {
                                
                                TreeNode file = new TreeNode();
                                file.Text = Path.GetFileName(f);
                                file.Tag = Path.GetFullPath(f);

                                countFiles++;

                                countFilesStatusLabel.Text = countFiles.ToString();
                                fileNameStatusLabel.Text = Path.GetFileName(f);
                                directoryTree.Invoke(new Action<TreeNode>((s) => node.Nodes.Add(s)), file);

                                statusSearch = true;
                            }
                        }
                        else
                        {
                            TreeNode file = new TreeNode();
                            file.Text = Path.GetFileName(f);
                            file.Tag = Path.GetFullPath(f);

                            countFiles++;

                            countFilesStatusLabel.Text = countFiles.ToString();
                            fileNameStatusLabel.Text = Path.GetFileName(f);
                            directoryTree.Invoke(new Action<TreeNode>((s) => node.Nodes.Add(s)), file);
                        }
                    }

                    if (fileNameBox.Text != "" && statusSearch)
                    {
                        directoryTree.Invoke(new Action<TreeNode>((s) => node.Nodes.Remove(s)), directory);
                    }
                }
            }
            catch (System.Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        /// <summary>
        /// Метод щелчка по узлу дерева
        /// </summary>
        /// <param name="sender">Объект узла дерева</param>
        /// <param name="e">Событие щелчка</param>
        private void ClickOnNode(object sender, TreeNodeMouseClickEventArgs e)
        {
            TreeNode selectedTreeNode = ((TreeView)sender).GetNodeAt(e.Location);

            if (sender is TreeView)
            {
                if (selectedTreeNode.Tag != null)
                {
                    string tag = selectedTreeNode.Tag.ToString();

                    using (FileStream fstream = File.OpenRead(tag))
                    {
                        // Преобразуем строку в байты и считываем данные из файла
                        byte[] array = new byte[fstream.Length];
                        fstream.Read(array, 0, array.Length);

                        // Декодируем байты в строку
                        string textFromFile = Encoding.Default.GetString(array);

                        contentBox.Text = textFromFile;
                        fileNameLabel.Text = tag;

                        fstream.Close();
                    }
                }
            }
        }

        /// <summary>
        /// Метод остановки потока поиска по нажатию кнопки buttonStop
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие клика</param>
        private void ClickOnButtonStop(object sender, EventArgs e)
        {
            searchThread.Abort();
            searchThread.Join();

            timer.Stop();

            buttonStop.Enabled = false;
            buttonSearch.Enabled = true;
            buttonPauseResume.Enabled = false;
            buttonContentSearch.Enabled = true;

            SetDefaultPause();
        }

        /// <summary>
        /// Метод зпуска поиска файлов в директории
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие клика</param>
        private void ClickOnButtonSearch(object sender, EventArgs e)
        {
            directoryTree.Nodes.Clear();
            StartSearch();
        }

        /// <summary>
        /// Метод установки паузы или продолжения потока
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие клика</param>
        private void ClickOnButtonPauseResume(object sender, EventArgs e)
        {
            if (!pause)
            {
                buttonPauseResume.Text = "▶";
                pause = true;
                timer.Stop();

                mrse.Reset();
            }
            else
            {
                buttonPauseResume.Text = "▌ ▌";
                pause = false;
                timer.Start();

                mrse.Set();
            }
        }

        /// <summary>
        /// Метод запуска потока поиска файлов с указанным содержимым
        /// </summary>
        /// <param name="sender">Объект кнопки</param>
        /// <param name="e">Событие клика</param>
        private void ClickOnButtonContentSearch(object sender, EventArgs e)
        {
            date = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, 0, 0, 0);
            timeStatusLabel.Text = date.ToString("HH:mm:ss");

            buttonStop.Enabled = true;
            buttonSearch.Enabled = false;
            buttonPauseResume.Enabled = true;
            buttonContentSearch.Enabled = false;

            SetDefaultPause();

            timer = new System.Windows.Forms.Timer()
            {
                Interval = 1000
            };
            timer.Tick += TickTimer;
            timer.Start();

            searchThread = new Thread(new ThreadStart(Find));
            searchThread.Start();
        }

        /// <summary>
        /// Метод запуска поиска файлов с указанным содержимым в потоке
        /// </summary>
        private void Find()
        {
            if (directoryTree.InvokeRequired)
            {
                TreeN.Clear();

                TreeNodeCollection nodes = null;
                string text = "";

                directoryTree.Invoke(new Action(() => nodes = directoryTree.Nodes));
                fileNameBox.Invoke(new Action(() => text = fileNameBox.Text));

                SetDefaultColor(nodes);

                Console.WriteLine(text);

                countFiles = 0;

                Find(nodes, text);

                /*foreach (TreeNode TN in TreeN)
                {
                    directoryTree.Invoke(new Action(() => directoryTree.Focus()));
                    directoryTree.Invoke(new Action<TreeNode>((s) => directoryTree.SelectedNode = s), TN);

                    Thread.Sleep(200);
                }*/

                fileNameStatusLabel.Text = "Поиск завершён";

                timer.Stop();

                buttonStop.BeginInvoke(new Action<bool>((s) => buttonStop.Enabled = s), false);
                buttonSearch.BeginInvoke(new Action<bool>((s) => buttonSearch.Enabled = s), true);
                buttonPauseResume.BeginInvoke(new Action<bool>((s) => buttonPauseResume.Enabled = s), false);
                buttonContentSearch.BeginInvoke(new Action<bool>((s) => buttonContentSearch.Enabled = s), true);
            }
        }

        List<TreeNode> TreeN = new List<TreeNode>();

        /// <summary>
        /// Мето поиска файлов с указанным содержимым
        /// </summary>
        /// <param name="nodes">Список узлов</param>
        /// <param name="content">Содержимое файла</param>
        void Find(TreeNodeCollection nodes, string content)
        {
            string cntText = "";
            fileContentNameBox.Invoke(new Action(() => cntText = fileContentNameBox.Text));

            foreach (TreeNode node in nodes)
            {
                if (node.Text.Contains(content) && node.Nodes.Count == 0)
                {
                    if (cntText != "")
                    {
                        TreeNode selectedTreeNode = node;

                        if (selectedTreeNode.Tag != null)
                        {
                            string tag = selectedTreeNode.Tag.ToString();
                            string text = "";

                            countFiles++;

                            countFilesStatusLabel.Text = countFiles.ToString();
                            fileNameStatusLabel.Text = selectedTreeNode.Text;

                            using (FileStream fstream = File.OpenRead(tag))
                            {
                                try
                                {
                                    // Преобразуем строку в байты и считываем данные из файла
                                    byte[] array = new byte[fstream.Length];
                                    fstream.Read(array, 0, array.Length);

                                    // Декодируем байты в строку
                                    string textFromFile = Encoding.Default.GetString(array);

                                    text = textFromFile;
                                    fstream.Close();
                                }
                                catch { }
                            }

                            if (text.Contains(cntText))
                            {
                                TreeN.Add(node);
                                contentBox.Invoke(new Action<string>((s) => contentBox.Text = s), text);
                                fileNameLabel.Invoke(new Action<string>((s) => fileNameLabel.Text = s), node.Tag);

                                node.BackColor = Color.Yellow;
                                directoryTree.Invoke(new Action<TreeNode>((s) => directoryTree.SelectedNode = s), node);
                            }
                            
                        }
                    }
                }

                if (node.Nodes.Count > 0)
                    Find(node.Nodes, content);
            }
        }

        /// <summary>
        /// Метод установки стандартных настроек паузы
        /// </summary>
        private void SetDefaultPause()
        {
            mrse.Set();
            pause = false;
            buttonPauseResume.Text = "▌ ▌";
        }

        /// <summary>
        /// Метод установки стандартного цвета узлам дерева
        /// </summary>
        /// <param name="nodes">Список узлов</param>
        private void SetDefaultColor(TreeNodeCollection nodes)
        {
            if (directoryTree.InvokeRequired)
            {
                foreach (TreeNode node in nodes)
                {
                    node.BackColor = Color.Empty;
                    SetDefaultColor(node.Nodes);
                }
            }
        }

        /// <summary>
        /// Метод сохранения данных при закрытии программы
        /// </summary>
        /// <param name="sender">Объект формы</param>
        /// <param name="e">Событие закрытия</param>
        private void CloseForm(object sender, FormClosedEventArgs e)
        {
            Properties.Settings.Default.defaultPath = drectoryNameBox.Text;
            Properties.Settings.Default.defaultFileName = fileNameBox.Text;
            Properties.Settings.Default.defaultText = fileContentNameBox.Text;
            Properties.Settings.Default.Save();
        }
    }
}
